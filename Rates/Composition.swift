import UIKit
import RatesKit

extension AppDelegate {

    static func compose() -> UIViewController {
        let session = URLSession(configuration: .default)
        let client = APIClientImpl(baseURL: "https://api.coindesk.com", urlSession: session)
        let gateway = RateGatewayImpl(apiClient: client)
        let store = RateStoreImpl(fileManager: FileManager())
        let interactor = RateInteractorImpl(rateGateway: gateway, rateStore: store)
        let properties = RateListViewProperties(
            error: NSLocalizedString("Error", comment: "Error alert title"),
            unknownError:  NSLocalizedString("OK", comment: "OK button"),
            errorFormatter: { $0.localizedDescription }
        )
        return RateListViewController(
            properties: properties,
            rateInteractor: interactor,
            notificationCenter: NotificationCenter.default
        )
    }
}

extension URL: ExpressibleByStringLiteral {
    public typealias StringLiteralType = StaticString

    public init(stringLiteral value: StringLiteralType) {
        self.init(string: value.description)!
    }
}
