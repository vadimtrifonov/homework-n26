import UIKit
import RatesKit

final class RateListView: UIView {
    private var currentRate: Rate?
    private var historicalRates = [Rate]()
    private let loadingCellID = String(describing: LoadingCell.self)
    private let rateCellID = String(describing: RateCell.self)

    private var allRates: [Rate] {
        return [currentRate].compactMap({ $0 }) + historicalRates
    }

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.allowsSelection = false
        tableView.dataSource = self
        tableView.register(LoadingCell.self, forCellReuseIdentifier: loadingCellID)
        tableView.register(RateCell.self, forCellReuseIdentifier: rateCellID)
        return tableView
    }()

    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.doesRelativeDateFormatting = true
        return formatter
    }()

    private let currencyFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale.current
        numberFormatter.numberStyle = .currency
        return numberFormatter
    }()

    init() {
        super.init(frame: .zero)

        addSubview(tableView)

        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: self.topAnchor),
            tableView.rightAnchor.constraint(equalTo: self.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: self.leftAnchor)
        ])
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func update(withCurrent rate: Rate) {
        currentRate = rate
        tableView.reloadData()
    }

    func update(withHistorical rates: [Rate]) {
        historicalRates = rates
        tableView.reloadData()
    }

    private func formatRate(_ rate: Decimal, withCurrencyCode currencyCode: String) -> String? {
        currencyFormatter.currencyCode = currencyCode
        return currencyFormatter.string(from: rate as NSDecimalNumber)
    }
}

extension RateListView: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allRates.isEmpty ? 1 : allRates.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard !allRates.isEmpty else {
            return tableView.dequeueReusableCell(withIdentifier: loadingCellID, for: indexPath)
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: rateCellID, for: indexPath)
        let rate = allRates[indexPath.row]
        cell.textLabel?.text = formatRate(rate.rate, withCurrencyCode: rate.currencyCode)
        cell.detailTextLabel?.text = dateFormatter.string(from: rate.date)
        return cell
    }
}

private final class LoadingCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.startAnimating()
        contentView.addSubview(indicator)

        indicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            indicator.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            indicator.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12)
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private final class RateCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
