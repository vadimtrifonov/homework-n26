public protocol RateInteractor {
    typealias TimerTick = (@escaping () -> Void) -> Void

    /// First returns a cached rate if available, then fetches a new one
    func currentRate(timerTick: TimerTick?, handler: @escaping (Result<Rate>) -> Void)
    /// Returns cached rates only, if they are yesterday's, otherwise fetches new
    func historicalRates(handler: @escaping (Result<[Rate]>) -> Void)

    /// Always fetches a new current rate
    func freshCurrentRate(timerTick: TimerTick?, handler: @escaping (Result<Rate>) -> Void)
}

public extension RateInteractor {

    func currentRate(handler: @escaping (Result<Rate>) -> Void) {
        currentRate(timerTick: nil, handler: handler)
    }

    func freshCurrentRate(handler: @escaping (Result<Rate>) -> Void) {
        freshCurrentRate(timerTick: nil, handler: handler)
    }
}
