# README

## iOS
* Continuously polls the current rate while in the foreground (a minute after
  successful fetch)
* Checks the current rate periodically in the background
* Checks historical rates on entering the foreground

## Watch
* Checks the current rate and historical rates on entering the foreground
* Checks the current rate periodically in the background

> Watch strategy has been changing with almost every release since inception.
It seems nowadays Apple envisions watch more as an independent ecosystem
(especially with the introduction of cellular connection). Thus, I haven't used
the watch connectivity framework and rely solely on the watch to retrieve
the data. However, my implementation is somewhat naive and doesn't utilise
background URLSession. A more production ready approach would be to periodically
download new historical rates in the background, but there is this limitation of
one application background refresh task (which is already used for the current
rate). I hypothesise that a potential solution would be to either rely on the
background session refresh or the watch connectivity refresh (triggered by the
iOS app when it has downloaded new historical rates).

## Caching
* Historical rates are cached in the `historical.json` file in the caches
  directory
* Historical rates are fetched anew only if they are older than yesterday's
* The current rate is cached in the `current.json` file
* The current rate is always fetched anew, but the cached one is returned first
  if it is today's

## Architecture
* RatesKit contains domain and data logic and shared between the apps

> For the production app I would consider dividing it into separate frameworks
for domain, data (depends on domain) and some foundation.
