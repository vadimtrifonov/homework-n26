import Foundation

public struct Rate: Codable, Equatable {
    public let date: Date
    public let rate: Decimal
    public let currencyCode: String

    public init(date: Date, rate: Decimal, currencyCode: String) {
        self.date = date
        self.rate = rate
        self.currencyCode = currencyCode
    }
}
