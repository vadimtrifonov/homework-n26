public protocol RateStore {
    func readCurrentRate(handler: @escaping (Rate?) -> Void)
    func saveCurrentRate(_ rate: Rate)
    func readHistoricalRates(handler: @escaping ([Rate]) -> Void)
    func saveHistoricalRates(_ rates: [Rate])
}
