import Foundation

public protocol APIClient {

    @discardableResult
    func get<T: Decodable>(
        path: String,
        queryItems: [URLQueryItem],
        handler: @escaping (Result<T>) -> Void
    ) -> URLSessionTask?
}

public extension APIClient {

    @discardableResult
    func get<T: Decodable>(
        path: String,
        handler: @escaping (Result<T>) -> Void
    ) -> URLSessionTask? {
        return get(path: path, queryItems: [], handler: handler)
    }
}
