import Foundation

final public class RateInteractorImpl: RateInteractor {
    private let rateGateway: RateGateway
    private let rateStore: RateStore

    public init(rateGateway: RateGateway, rateStore: RateStore) {
        self.rateGateway = rateGateway
        self.rateStore = rateStore
    }

    public func currentRate(
        timerTick: TimerTick?,
        handler: @escaping (Result<Rate>) -> Void
    ) {
        rateStore.readCurrentRate { [weak self] rate in
            guard let _self = self else {
                return
            }

            guard let rate = rate else {
                return _self.freshCurrentRate(timerTick: timerTick, handler: handler)
            }

            if rate.date >= Date.today {
                handler(.success(rate))
            }

            _self.freshCurrentRate(timerTick: timerTick, handler: handler)
        }
    }

    public func historicalRates(handler: @escaping (Result<[Rate]>) -> Void) {
        rateStore.readHistoricalRates { [weak self] rates in
            guard let _self = self else {
                return
            }

            guard !rates.isEmpty else {
                return _self.freshHistoricalRates(handler: handler)
            }

            handler(.success(rates))

            if let date = rates.first?.date, date < Date.yesterday {
                _self.freshHistoricalRates(handler: handler)
            }
        }
    }

    public func freshCurrentRate(
        timerTick: TimerTick?,
        handler: @escaping (Result<Rate>) -> Void
    ) {
        rateGateway.fetchCurrentRate(currencyCode: "USD") { [weak self, rateStore] result in
            result.value.map(rateStore.saveCurrentRate)
            handler(result)

            if case let (.success, timerTick?) = (result, timerTick) {
                timerTick {
                    self?.freshCurrentRate(timerTick: timerTick, handler: handler)
                }
            }
        }
    }

    private func freshHistoricalRates(handler: @escaping (Result<[Rate]>) -> Void) {
        rateGateway.fetchRates(
            currencyCode: "USD",
            start: Date.twoWeeksAgo,
            end: Date.yesterday
        ) { [rateStore] result in
            result.value.map(rateStore.saveHistoricalRates)
            handler(result)
        }
    }
}

internal extension Calendar {

    static var gregorianGMT: Calendar {
        guard let timeZone = TimeZone(secondsFromGMT: 0) else {
            fatalError("Expected to always succeed")
        }
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = timeZone
        return calendar
    }
}

internal extension Date {

    static var today: Date {
        return Calendar.gregorianGMT.startOfDay(for: Date())
    }

    static var yesterday: Date {
        guard let date = Calendar.gregorianGMT.date(byAdding: .day, value: -1, to: .today) else {
            fatalError("Expected to always succeed")
        }
        return date
    }

    static var twoWeeksAgo: Date {
        guard let date = Calendar.gregorianGMT.date(
            byAdding: .weekOfYear,
            value: -2,
            to: .today
        ) else {
            fatalError("Expected to always succeed")
        }
        return date
    }
}
