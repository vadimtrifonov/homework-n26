import UIKit
import RatesKit

public struct RateListViewProperties {
    let error: String
    let ok: String
    let errorFormater: (Error) -> String

    public init(error: String, unknownError: String, errorFormatter: @escaping (Error) -> String) {
        self.error = error
        self.ok = unknownError
        self.errorFormater = errorFormatter
    }
}

public final class RateListViewController: UIViewController {
    private let properties: RateListViewProperties
    private let rateInteractor: RateInteractor
    private let notificationCenter: NotificationCenter
    private lazy var _view = RateListView()
    private var observer: NSObjectProtocol?

    public init(
        properties: RateListViewProperties,
        rateInteractor: RateInteractor,
        notificationCenter: NotificationCenter
    ) {
        self.properties = properties
        self.rateInteractor = rateInteractor
        self.notificationCenter = notificationCenter
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func loadView() {
        view = _view
    }

    override public func viewDidLoad() {
        super.viewDidLoad()

        continuouslyUpdateCurrentRate()
        updateHistoricalRates()

        observer = notificationCenter.addObserver(
            forName: .UIApplicationWillEnterForeground,
            object: nil,
            queue: nil)
        { [weak self] _ in
            self?.updateHistoricalRates()
        }
    }

    private func continuouslyUpdateCurrentRate() {
        let timerTick: RateInteractor.TimerTick = { tock in
            Timer.scheduledTimer(withTimeInterval: 60, repeats: false, block: { _ in tock() })
        }

        rateInteractor.currentRate(timerTick: timerTick) { [weak self] result in
            do {
                try self?._view.update(withCurrent: result.unwrap())
            } catch {
                self?.presentErrorAlert(error)
            }
        }
    }

    private func updateHistoricalRates() {
        rateInteractor.historicalRates { [weak self] result in
            do {
                try self?._view.update(withHistorical: result.unwrap())
            } catch {
                self?.presentErrorAlert(error)
            }
        }
    }

    private func presentErrorAlert(_ error: Error) {
        let alert = UIAlertController(
            title: properties.error,
            message: properties.errorFormater(error),
            preferredStyle: .alert
        )
        let ok = UIAlertAction(title: properties.ok, style: .default, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true)
    }

    deinit {
        observer.map(notificationCenter.removeObserver)
    }
}

extension RateListViewController {

    public func updateCurrentRate(handler: @escaping (UIBackgroundFetchResult) -> Void) {
        rateInteractor.freshCurrentRate { [weak self] result in
            do {
                try self?._view.update(withCurrent: result.unwrap())
                handler(.newData)
            } catch {
                handler(.failed)
            }
        }
    }
}
