import Foundation

public enum RateGatewayError: Error {
    case invalidDateFormat
    case invalidCurrencyResponse
}

public final class RateGatewayImpl: RateGateway {
    private let apiClient: APIClient
    private lazy var timestampFormatter = ISO8601DateFormatter()

    private lazy var dateFormatter: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate, .withDashSeparatorInDate]
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter
    }()

    public init(apiClient: APIClient) {
        self.apiClient = apiClient
    }

    public func fetchCurrentRate(
        currencyCode: String,
        handler: @escaping (Result<Rate>) -> Void
    ) {
        apiClient.get(
            path: "/v1/bpi/currentprice/\(currencyCode).json"
        ) { [weak self] (result: Result<CurrentRateResponse>) in
            guard let _self = self else {
                return
            }
            let result = result.flatMap { response in
                try _self.makeRate(from: response, withCurrencyCode: currencyCode)
            }
            handler(result)
        }
    }

    public func fetchRates(
        currencyCode: String,
        start: Date,
        end: Date,
        handler: @escaping (Result<[Rate]>) -> Void
    ) {
        let queryItems = [
            URLQueryItem(name: "currency", value: currencyCode),
            URLQueryItem(name: "start", value: dateFormatter.string(from: start)),
            URLQueryItem(name: "end", value: dateFormatter.string(from: end))
        ]

        apiClient.get(
            path: "/v1/bpi/historical/close.json",
            queryItems: queryItems
        ) { [weak self] (result: Result<RatesResponse>) in
            guard let _self = self else {
                return
            }
            let result = result.flatMap { response in
                try _self.makeRates(from: response, withCurrencyCode: currencyCode)
                    .sorted(by: { $0.date > $1.date })
            }
            handler(result)
        }
    }

    private func makeRate(
        from response: CurrentRateResponse,
        withCurrencyCode code: String
    ) throws -> Rate {
        guard let date = timestampFormatter.date(from: response.time.updatedISO) else {
            throw RateGatewayError.invalidDateFormat
        }
        guard let rate = response.bpi.first(where: { $0.key == code })?.value.rate_float else {
            throw RateGatewayError.invalidCurrencyResponse
        }
        return Rate(date: date, rate: rate, currencyCode: code)
    }

    private func makeRates(
        from response: RatesResponse,
        withCurrencyCode code: String
    ) throws -> [Rate] {
        return try response.bpi.map { dateString, rate in
            guard let date = dateFormatter.date(from: dateString) else {
                throw RateGatewayError.invalidDateFormat
            }
            return Rate(date: date, rate: rate, currencyCode: code)
        }
    }
}

internal struct RatesResponse: Decodable {
    let bpi: [String: Decimal]
}

internal struct CurrentRateResponse: Decodable {

    struct Time: Decodable {
        let updatedISO: String
    }

    struct Rate: Decodable {
        let rate_float: Decimal
    }

    let time: Time
    let bpi: [String: Rate]
}
