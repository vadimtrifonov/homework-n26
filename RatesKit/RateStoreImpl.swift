import Foundation
import os.log

public final class RateStoreImpl: RateStore {
    private let fileManager: FileManager

    private var currentRateURL: URL  {
        let url = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        return url.appendingPathComponent("current.json")
    }

    private var historicalRatesURL: URL  {
        let url = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        return url.appendingPathComponent("historical.json")
    }

    public init(fileManager: FileManager) {
        self.fileManager = fileManager
    }

    public func readCurrentRate(handler: @escaping (Rate?) -> Void) {
        read(contentsOf: currentRateURL, handler: handler)
    }

    public func saveCurrentRate(_ rate: Rate) {
        save(to: currentRateURL, contents: rate)
    }

    public func readHistoricalRates(handler: @escaping ([Rate]) -> Void) {
        read(contentsOf: historicalRatesURL) { (rate: [Rate]?) in
            handler(rate ?? [])
        }
    }

    public func saveHistoricalRates(_ rates: [Rate]) {
        save(to: historicalRatesURL, contents: rates)
    }

    private func read<T: Decodable>(contentsOf url: URL, handler: @escaping (T?) -> Void){
        DispatchQueue.global(qos: .userInitiated).async { [fileManager] in
            do {
                guard fileManager.fileExists(atPath: url.path) else {
                    return handler(nil)
                }

                let data = try Data(contentsOf: url, options: [])
                let rates = try JSONDecoder().decode(T.self, from: data)

                DispatchQueue.main.async {
                    handler(rates)
                }
            } catch {
                #if DEBUG
                    os_log("Failed to read with error: %@", error.localizedDescription)
                #endif
            }
        }
    }

    private func save<T: Encodable>(to url: URL, contents: T) {
        DispatchQueue.global(qos: .default).async {
            do {
                let data = try JSONEncoder().encode(contents)
                try data.write(to: url)
            } catch {
                #if DEBUG
                    os_log("Failed to save with error: %@", error.localizedDescription)
                #endif
            }
        }
    }
}
