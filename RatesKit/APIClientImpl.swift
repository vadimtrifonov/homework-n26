import Foundation
import os.log

public enum APIClientError: Error {
    case malformedURL
    case unknownError
}

public final class APIClientImpl: APIClient {
    private let baseURL: URL
    private let session: URLSession

    public init(baseURL: URL, urlSession: URLSession) {
        self.baseURL = baseURL
        self.session = urlSession
    }

    @discardableResult
    public func get<T: Decodable>(
        path: String,
        queryItems: [URLQueryItem],
        handler: @escaping (Result<T>) -> Void
    ) -> URLSessionTask? {
        var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: false)
        components?.path = path
        components?.queryItems = queryItems

        guard let url = components?.url else {
            handler(.failure(APIClientError.malformedURL))
            return nil
        }

        #if DEBUG
            os_log("%@ REQUEST: %@", url.path, url.query ?? "")
        #endif

        let request = URLRequest(url: url)

        let task = session.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                #if DEBUG
                    os_log("%@ ERROR: %@", url.path, error?.localizedDescription ?? "Unknown")
                #endif
                return handler(.failure(error ?? APIClientError.unknownError))
            }

            #if DEBUG
                os_log("%@ RESPONSE: %@", url.path, String(data: data, encoding: .utf8) ?? "")
            #endif

            do {
                let response = try JSONDecoder().decode(T.self, from: data)
                DispatchQueue.main.async {
                    handler(.success(response))
                }
            } catch {
                #if DEBUG
                    os_log("%@ DECODING ERROR: %@", url.path, error.localizedDescription)
                #endif
                DispatchQueue.main.async {
                    handler(.failure(error))
                }
            }
        }

        task.resume()
        return task
    }
}
