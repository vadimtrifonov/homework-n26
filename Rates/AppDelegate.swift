import UIKit
import RatesKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
    ) -> Bool {
        application.setMinimumBackgroundFetchInterval(UIApplicationBackgroundFetchIntervalMinimum)

        let window = UIWindow()
        window.rootViewController = AppDelegate.compose()
        window.makeKeyAndVisible()
        self.window = window

        return true
    }

    func application(
        _ application: UIApplication,
        performFetchWithCompletionHandler handler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        guard let controller = window?.rootViewController as? RateListViewController else {
            return handler(.noData)
        }
        controller.updateCurrentRate(handler: handler)
    }
}
