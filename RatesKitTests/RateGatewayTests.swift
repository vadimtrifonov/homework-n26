import XCTest
@testable import RatesKit

class RateGatewayTests: XCTestCase {
    private var client: FakeAPIClient!
    private var gateway: RateGateway!

    override func setUp() {
        super.setUp()

        client = FakeAPIClient()
        gateway = RateGatewayImpl(apiClient: client)
    }

    func testFormatsDatesToISODates() {
        let fetchExpectation = expectation(description: "")
        client.ratesResponse = RatesResponse(bpi: [:])

        gateway.fetchRates(
            currencyCode: "EUR",
            start: Date("2018-05-20"),
            end: Date("2018-05-18")
        ) { [unowned self] result in
            XCTAssertEqual(
                self.client.queryItemsCaptor.first(where: { $0.name == "start" })?.value,
                "2018-05-20"
            )
            XCTAssertEqual(
                self.client.queryItemsCaptor.first(where: { $0.name == "end" })?.value,
                "2018-05-18"
            )
            fetchExpectation.fulfill()
        }

        wait(for: [fetchExpectation], timeout: 0.1)
    }

    func testConvertsRatesReponseToRatesAndSortsThemByDate() {
        let fetchExpectation = expectation(description: "")
        client.ratesResponse = RatesResponse(bpi: [
            "2018-05-18": 100,
            "2018-05-20": 300,
            "2018-05-19": 200
        ])
        let rate1 = Rate(date: Date("2018-05-20"), rate: 300, currencyCode: "EUR")
        let rate2 = Rate(date: Date("2018-05-19"), rate: 200, currencyCode: "EUR")
        let rate3 = Rate(date: Date("2018-05-18"), rate: 100, currencyCode: "EUR")

        gateway.fetchRates(currencyCode: "EUR", start: Date.today, end: Date.yesterday) { result in
            XCTAssertEqual(result.value, [rate1, rate2, rate3])
            fetchExpectation.fulfill()
        }

        wait(for: [fetchExpectation], timeout: 0.1)
    }

    func testReturnsErrorInCaseOfInvalidRatesDateFormat() {
        let fetchExpectation = expectation(description: "")
        client.ratesResponse = RatesResponse(bpi: ["05/18/2010": 100])

        gateway.fetchRates(currencyCode: "EUR", start: Date.today, end: Date.yesterday) { result in
            XCTAssertEqual(result.error as? RateGatewayError, .invalidDateFormat)
            fetchExpectation.fulfill()
        }

        wait(for: [fetchExpectation], timeout: 0.1)
    }

    func testConvertsCurrentRateReponseToRate() {
        let fetchExpectation = expectation(description: "")
        client.currentRateResponse = CurrentRateResponse(
            time: CurrentRateResponse.Time(updatedISO: "2018-05-20T00:00:00+00:00"),
            bpi: ["EUR": CurrentRateResponse.Rate(rate_float: 100)]
        )
        let rate = Rate(date: Date("2018-05-20"), rate: 100, currencyCode: "EUR")

        gateway.fetchCurrentRate(currencyCode: "EUR") { result in
            XCTAssertEqual(result.value, rate)
            fetchExpectation.fulfill()
        }

        wait(for: [fetchExpectation], timeout: 0.1)
    }

    func testReturnsErrorInCaseOfInvalidCurrentRateDateFormat() {
        let fetchExpectation = expectation(description: "")
        client.currentRateResponse = CurrentRateResponse(
            time: CurrentRateResponse.Time(updatedISO: "05/20/2018"),
            bpi: ["EUR": CurrentRateResponse.Rate(rate_float: 100)]
        )

        gateway.fetchCurrentRate(currencyCode: "EUR") { result in
            XCTAssertEqual(result.error as? RateGatewayError, .invalidDateFormat)
            fetchExpectation.fulfill()
        }

        wait(for: [fetchExpectation], timeout: 0.1)
    }

    func testReturnsErrorInCaseOfInvalidCurrency() {
        let fetchExpectation = expectation(description: "")
        client.currentRateResponse = CurrentRateResponse(
            time: CurrentRateResponse.Time(updatedISO: "2018-05-20T00:00:00+00:00"),
            bpi: ["USD": CurrentRateResponse.Rate(rate_float: 100)]
        )

        gateway.fetchCurrentRate(currencyCode: "EUR") { result in
            XCTAssertEqual(result.error as? RateGatewayError, .invalidCurrencyResponse)
            fetchExpectation.fulfill()
        }

        wait(for: [fetchExpectation], timeout: 0.1)
    }
}

private final class FakeAPIClient: APIClient {
    var currentRateResponse: CurrentRateResponse?
    var ratesResponse: RatesResponse?
    var queryItemsCaptor = [URLQueryItem]()

    @discardableResult
    func get<T: Decodable>(
        path: String,
        queryItems: [URLQueryItem],
        handler: @escaping (Result<T>) -> Void
    ) -> URLSessionTask? {
        queryItemsCaptor = queryItems
        (currentRateResponse as? T ?? ratesResponse as? T).map(Result.success).map(handler)
        return nil
    }
}

private extension Date {

    init(_ string: String) {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate, .withDashSeparatorInDate]
        formatter.timeZone = TimeZone(secondsFromGMT: 0)

        guard let date = formatter.date(from: string) else {
            fatalError("Expected test string dates to be always valid")
        }

        self.init(timeInterval: 0, since: date)
    }
}
