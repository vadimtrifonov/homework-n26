import XCTest
@testable import RatesKit

class CurrentRateInteractorTests: XCTestCase {
    private var gateway: FakeRateGateway!
    private var store: FakeRateStore!
    private var interactor: RateInteractor!

    override func setUp() {
        super.setUp()

        gateway = FakeRateGateway()
        store = FakeRateStore()
        interactor = RateInteractorImpl(rateGateway: gateway, rateStore: store)
    }

    func testPollsCurrentRateUsingTimerTick() {
        let fetchExpectation1 = expectation(description: "First immediate fetch")
        let fetchExpectation2 = expectation(description: "Second polling fetch")
        let rate = Rate(date: Date(), rate: 100, currencyCode: "EUR")
        var continueTicks = true
        let timerTick: RateInteractor.TimerTick = { continueTicks ? $0() : () }

        gateway.currentRate = rate

        interactor.currentRate(timerTick: timerTick) { [unowned self] result in
            switch self.gateway.fetchCurrentRateCalled {
            case 1:
                XCTAssertEqual(result.value, rate)
                fetchExpectation1.fulfill()
            case 2:
                XCTAssertEqual(result.value, rate)
                fetchExpectation2.fulfill()
                continueTicks = false
            default:
                XCTFail("Failed to first fetch current rate and then poll another")
            }
        }

        wait(for: [fetchExpectation1, fetchExpectation2], timeout: 0.1, enforceOrder: true)
    }

    func testNeverStartsPollingIfFirstFetchFails() {
        let fetchExpectation1 = expectation(description: "First immediate fetch with error")
        let fetchExpectation2 = expectation(description: "Second polling fetch should never happen")
        var continueTicks = true
        let timerTick: RateInteractor.TimerTick = { continueTicks ? $0() : () }

        fetchExpectation2.isInverted = true

        interactor.currentRate(timerTick: timerTick) { [unowned self] result in
            switch self.gateway.fetchCurrentRateCalled {
            case 1:
                XCTAssertEqual(
                    result.error as? FakeRateGateway.Error,
                    FakeRateGateway.Error.currentRateError
                )
                fetchExpectation1.fulfill()
            case 2:
                fetchExpectation2.fulfill()
            default:
                continueTicks = false
            }
        }

        wait(for: [fetchExpectation1, fetchExpectation2], timeout: 0.1, enforceOrder: true)
    }

    func testStopsPollingIfFetchFails() {
        let fetchExpectation1 = expectation(description: "First immediate fetch")
        let fetchExpectation2 = expectation(description: "Second polling fetch with error")
        let fetchExpectation3 = expectation(description: "Third polling fetch should never happen")
        var continueTicks = true
        let timerTick: RateInteractor.TimerTick = { continueTicks ? $0() : () }

        fetchExpectation3.isInverted = true
        gateway.currentRate = Rate(date: Date(), rate: 0, currencyCode: "")

        interactor.currentRate(timerTick: timerTick) { [unowned self] result in
            switch self.gateway.fetchCurrentRateCalled {
            case 1:
                fetchExpectation1.fulfill()
                self.gateway.currentRate = nil
            case 2:
                fetchExpectation2.fulfill()
            case 3:
                fetchExpectation3.fulfill()
            default:
                continueTicks = false
            }
        }

        wait(
            for: [fetchExpectation1, fetchExpectation2, fetchExpectation3],
            timeout: 0.1,
            enforceOrder: true
        )
    }

    func testFetchesCurrentRateIfNothingCached() {
        let fetchExpectation = expectation(description: "Fetches a new rate")
        let rate = Rate(date: Date(), rate: 100, currencyCode: "USD")

        gateway.currentRate = rate

        interactor.currentRate { [unowned self] result in
            XCTAssertEqual(result.value, rate)
            XCTAssertEqual(self.store.readCalled, 1)
            XCTAssertEqual(self.gateway.fetchCurrentRateCalled, 1)
            XCTAssertEqual(self.store.saveCalled, 1)
            fetchExpectation.fulfill()
        }

        wait(for: [fetchExpectation], timeout: 0.1)
    }

    func testIgnoresCachedRateIfOlderThanTodays() {
        let fetchExpectation = expectation(description: "Fetches a new rate")
        let cachedRate = Rate(date: Date.yesterday, rate: 200, currencyCode: "USD")
        let rate = Rate(date: Date(), rate: 300, currencyCode: "USD")

        store.currentRate = cachedRate
        gateway.currentRate = rate

        interactor.currentRate { [unowned self] result in
            XCTAssertEqual(result.value, rate)
            XCTAssertEqual(self.store.readCalled, 1)
            XCTAssertEqual(self.gateway.fetchCurrentRateCalled, 1)
            XCTAssertEqual(self.store.saveCalled, 1)
            fetchExpectation.fulfill()
        }

        wait(for: [fetchExpectation], timeout: 0.1)
    }

    func testReturnsCachedRateAndFetchesNewIfCachedIsTodays() {
        let readExpectation = expectation(description: "Returns the cached rate")
        let fetchExpectation = expectation(description: "Fetches a new rate")
        let cachedRate = Rate(date: Date.today, rate: 200, currencyCode: "USD")
        let rate = Rate(date: Date(), rate: 300, currencyCode: "USD")

        store.currentRate = cachedRate
        gateway.currentRate = rate

        interactor.currentRate { [unowned self] result in
            switch result.value {
            case cachedRate:
                XCTAssertEqual(self.store.readCalled, 1)
                XCTAssertEqual(self.gateway.fetchCurrentRateCalled, 0)
                XCTAssertEqual(self.store.saveCalled, 0)
                readExpectation.fulfill()
            case rate:
                XCTAssertEqual(self.store.readCalled, 1)
                XCTAssertEqual(self.gateway.fetchCurrentRateCalled, 1)
                XCTAssertEqual(self.store.saveCalled, 1)
                fetchExpectation.fulfill()
            default:
                XCTFail("Failed to first return the cached rate and then fetch a new rate")
            }
        }

        wait(for: [readExpectation, fetchExpectation], timeout: 0.1, enforceOrder: true)
    }
}

private final class FakeRateGateway: RateGateway {
    enum Error: Swift.Error {
        case currentRateError
    }

    var currentRate: Rate?
    var fetchCurrentRateCalled = 0

    func fetchRates(
        currencyCode: String,
        start: Date,
        end: Date,
        handler: @escaping (Result<[Rate]>
    ) -> Void) {}

    func fetchCurrentRate(currencyCode: String, handler: @escaping (Result<Rate>) -> Void) {
        fetchCurrentRateCalled += 1
        currentRate.map(Result.success).map(handler) ?? handler(.failure(Error.currentRateError))
    }
}

private final class FakeRateStore: RateStore {
    var currentRate: Rate?
    var readCalled = 0
    var saveCalled = 0

    func readCurrentRate(handler: @escaping (Rate?) -> Void) {
        readCalled += 1
        handler(currentRate)
    }

    func saveCurrentRate(_ rate: Rate) {
        saveCalled += 1
    }

    func readHistoricalRates(handler: @escaping ([Rate]) -> Void) {}
    func saveHistoricalRates(_ rates: [Rate]) {}
}
