import XCTest
@testable import RatesKit

class HistoricalRatesInteractorTests: XCTestCase {
    private var gateway: FakeRateGateway!
    private var store: FakeRateStore!
    private var interactor: RateInteractor!

    override func setUp() {
        super.setUp()

        gateway = FakeRateGateway()
        store = FakeRateStore()
        interactor = RateInteractorImpl(rateGateway: gateway, rateStore: store)
    }

    func testFetchesNewRatesIfNothingCached() {
        let fetchExpectation = expectation(description: "Fetches new rates")
        let rate = Rate(date: Date.today, rate: 100, currencyCode: "USD")

        gateway.historicalRates = [rate]

        interactor.historicalRates { [unowned self] result in
            XCTAssertEqual(result.value, [rate])
            XCTAssertEqual(self.gateway.fetchRatesCalled, 1)
            XCTAssertEqual(self.store.readCalled, 1)
            XCTAssertEqual(self.store.saveCalled, 1)
            fetchExpectation.fulfill()
        }

        wait(for: [fetchExpectation], timeout: 0.1)
    }

    func testReturnsCachedRatesIfTopCachedRateIsYesterdays() {
        let readExpectation = expectation(description: "Returns cached rates")
        let cachedRate1 = Rate(date: Date.yesterday, rate: 200, currencyCode: "USD")
        let cachedRate2 = Rate(date: Date.dayBeforeYesterday, rate: 100, currencyCode: "USD")
        let rate = Rate(date: Date.today, rate: 300, currencyCode: "USD")

        store.rates = [cachedRate1, cachedRate2]
        gateway.historicalRates = [rate]

        interactor.historicalRates { [unowned self] result in
            XCTAssertEqual(result.value, [cachedRate1, cachedRate2])
            XCTAssertEqual(self.store.readCalled, 1)
            XCTAssertEqual(self.gateway.fetchRatesCalled, 0)
            XCTAssertEqual(self.store.saveCalled, 0)
            readExpectation.fulfill()
        }

        wait(for: [readExpectation], timeout: 0.1)
    }

    func testReturnsCachedRatesAndFetchesNewIfCachedAreOlderThanYesterdays() {
        let readExpectation = expectation(description: "Returns cached rates")
        let fetchExpectation = expectation(description: "Fetches new rates")
        let cachedRate = Rate(date: Date.dayBeforeYesterday, rate: 100, currencyCode: "USD")
        let rate = Rate(date: Date.today, rate: 200, currencyCode: "USD")

        store.rates = [cachedRate]
        gateway.historicalRates = [rate]

        interactor.historicalRates { [unowned self] result in
            switch result.value {
            case [cachedRate]:
                XCTAssertEqual(self.store.readCalled, 1)
                XCTAssertEqual(self.gateway.fetchRatesCalled, 0)
                XCTAssertEqual(self.store.saveCalled, 0)
                readExpectation.fulfill()
            case [rate]:
                XCTAssertEqual(self.store.readCalled, 1)
                XCTAssertEqual(self.gateway.fetchRatesCalled, 1)
                XCTAssertEqual(self.store.saveCalled, 1)
                fetchExpectation.fulfill()
            default:
                XCTFail("Failed to first return cached rates and then fetch new rates")
            }
        }

        wait(for: [readExpectation, fetchExpectation], timeout: 0.1, enforceOrder: true)
    }
}

private final class FakeRateGateway: RateGateway {
    var historicalRates = [Rate]()
    var fetchRatesCalled = 0

    func fetchRates(
        currencyCode: String,
        start: Date,
        end: Date,
        handler: @escaping (Result<[Rate]>
    ) -> Void) {
        fetchRatesCalled += 1
        handler(.success(historicalRates))
    }

    func fetchCurrentRate(currencyCode: String, handler: @escaping (Result<Rate>) -> Void) {}
}

private final class FakeRateStore: RateStore {
    var rates = [Rate]()
    var readCalled = 0
    var saveCalled = 0

    func readHistoricalRates(handler: @escaping ([Rate]) -> Void) {
        readCalled += 1
        handler(rates)
    }

    func saveHistoricalRates(_ rates: [Rate]) {
        saveCalled += 1
    }

    func readCurrentRate(handler: @escaping (Rate?) -> Void) {}
    func saveCurrentRate(_ rate: Rate) {}
}

private extension Date {

    static var dayBeforeYesterday: Date {
        guard let date = Calendar.gregorianGMT.date(byAdding: .day, value: -2, to: .today) else {
            fatalError("Expected to always succeed")
        }
        return date
    }
}
