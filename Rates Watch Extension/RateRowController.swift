import WatchKit

final class RateRowController: NSObject {
    @IBOutlet weak var date: WKInterfaceLabel!
    @IBOutlet weak var rate: WKInterfaceLabel!
}
