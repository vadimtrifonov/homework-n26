import WatchKit

public final class RateListInterfaceController: WKInterfaceController {
    @IBOutlet var table: WKInterfaceTable!

    private var currentRate: Rate?
    private var historicalRates = [Rate]()

    private var allRates: [Rate] {
        return [currentRate].compactMap({ $0 }) + historicalRates
    }

    private let interactor: RateInteractor = {
        let session = URLSession(configuration: .default)
        let client = APIClientImpl(baseURL: "https://api.coindesk.com", urlSession: session)
        let gateway = RateGatewayImpl(apiClient: client)
        let store = RateStoreImpl(fileManager: FileManager())
        let interactor = RateInteractorImpl(rateGateway: gateway, rateStore: store)
        return interactor
    }()

    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.doesRelativeDateFormatting = true
        return formatter
    }()

    private let currencyFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale.current
        numberFormatter.numberStyle = .currency
        return numberFormatter
    }()

    override public func awake(withContext context: Any?) {
        super.awake(withContext: context)

        configureLoadingRow()
        updateCurrentRate()
        upadateHistoricalRates()
    }

    public func willEnterForeground() {
        updateCurrentRate()
        upadateHistoricalRates()
    }

    private func updateCurrentRate() {
        interactor.currentRate { [weak self] result in
            do {
                self?.currentRate = try result.unwrap()
                self?.updateRows()
            }
            catch {
                self?.presenErrorAlert(error)
            }
        }
    }

    private func upadateHistoricalRates() {
        interactor.historicalRates { [weak self] result in
            do {
                self?.historicalRates = try result.unwrap()
                self?.updateRows()
            }
            catch {
                self?.presenErrorAlert(error)
            }
        }
    }

    private func configureLoadingRow() {
        table.setNumberOfRows(1, withRowType: "RateRow")
        let controller = table.rowController(at: 0) as? RateRowController
        controller?.date.setText(NSLocalizedString("Today", comment: "Today's date placeholder"))
        controller?.rate.setText(NSLocalizedString("Loading...", comment: "Rate placeholder"))
    }

    private func updateRows() {
        table.setNumberOfRows(allRates.count, withRowType: "RateRow")
        for (index, rate) in allRates.enumerated() {
            let controller = table.rowController(at: index) as? RateRowController
            controller?.date.setText(dateFormatter.string(from: rate.date))
            controller?.rate.setText(formatRate(rate.rate, withCurrencyCode: rate.currencyCode))
        }
    }

    private func formatRate(_ rate: Decimal, withCurrencyCode currencyCode: String) -> String? {
        currencyFormatter.currencyCode = currencyCode
        return currencyFormatter.string(from: rate as NSDecimalNumber)
    }


    private func presenErrorAlert(_ error: Error) {
        let ok = WKAlertAction(
            title: NSLocalizedString("OK", comment: "OK button"),
            style: .default,
            handler: {}
        )
        presentAlert(
            withTitle: NSLocalizedString("Error", comment: "Error alert title"),
            message: error.localizedDescription,
            preferredStyle: .alert,
            actions: [ok]
        )
    }
}

extension RateListInterfaceController {

    public func updateCurrentRate(task: WKApplicationRefreshBackgroundTask) {
        interactor.freshCurrentRate { [weak self] result in
            do {
                self?.currentRate = try result.unwrap()
                self?.updateRows()
                RateListInterfaceController.scheduleCurrentRateUpdate()
                task.setTaskCompletedWithSnapshot(true)
            }
            catch {
                RateListInterfaceController.scheduleCurrentRateUpdate()
                task.setTaskCompletedWithSnapshot(false)
            }
        }
    }

    public static func scheduleCurrentRateUpdate() {
        WKExtension.shared().scheduleBackgroundRefresh(
            withPreferredDate: Date(timeIntervalSinceNow: 60 * 60),
            userInfo: "CurrentRate" as NSString,
            scheduledCompletion: { _ in }
        )
    }
}

extension URL: ExpressibleByStringLiteral {
    public typealias StringLiteralType = StaticString

    public init(stringLiteral value: StringLiteralType) {
        self.init(string: value.description)!
    }
}
