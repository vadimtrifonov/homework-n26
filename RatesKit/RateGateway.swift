import Foundation

public protocol RateGateway {

    func fetchCurrentRate(
        currencyCode: String,
        handler: @escaping (Result<Rate>) -> Void
    )

    func fetchRates(
        currencyCode: String,
        start: Date,
        end: Date,
        handler: @escaping (Result<[Rate]>) -> Void
    )
}
