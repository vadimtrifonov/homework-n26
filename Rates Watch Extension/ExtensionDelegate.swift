import WatchKit

class ExtensionDelegate: NSObject, WKExtensionDelegate {

    func applicationDidFinishLaunching() {
        RateListInterfaceController.scheduleCurrentRateUpdate()
    }

    func applicationWillEnterForeground() {
        if let controller = WKExtension.shared().rootInterfaceController
            as? RateListInterfaceController
        {
            controller.willEnterForeground()
        }
    }

    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
        for task in backgroundTasks {
            switch task {
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
                handleApplicationRefresh(backgroundTask)

            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
                snapshotTask.setTaskCompleted(
                    restoredDefaultState: true,
                    estimatedSnapshotExpiration: Date.distantFuture,
                    userInfo: nil
                )

            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
                connectivityTask.setTaskCompletedWithSnapshot(false)

            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
                urlSessionTask.setTaskCompletedWithSnapshot(false)

            default:
                task.setTaskCompletedWithSnapshot(false)
            }
        }
    }

    private func handleApplicationRefresh(_ task: WKApplicationRefreshBackgroundTask) {
        guard let controller = WKExtension.shared().rootInterfaceController
            as? RateListInterfaceController
        else {
             return task.setTaskCompletedWithSnapshot(false)
        }
        controller.updateCurrentRate(task: task)
    }
}
